<?php

$height = [180,200,156,220,145];
$surnames = ['Baatyrbekov','Pupkin','Pitt','Smith','Jackson'];
$max = max($height);
$max_height_player = 0;
print "Players height list\n";
print " № |   Player   | Height \n";
print "---+------------+--------\n";
for ($i = 0; $i < count($height); $i++) {
    echo sprintf ("%-3d|%-12s|%-8d\n",   $i+1,$surnames[$i],$height[$i]);
    if($height[$i] == $max){
        $max_height_player = $surnames[$i];
    }
}
print "\nTallest player is ".$max_height_player." with height ".$max."\n";
